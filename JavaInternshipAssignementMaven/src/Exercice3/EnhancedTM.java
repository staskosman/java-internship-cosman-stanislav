package Exercice3;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class EnhancedTM {
	
	//this method can create user
		private static void createUser(String[] args, Connection connection) {
			//read second, third and fourth argument
			String fnA = args[1];
			String lnA = args[2];
			String unA = args[3];
			
			String fn = "";
			String ln = "";
			String un = "";
			//select only the first name, not all argument (e.g. if argument is this "-fn='test'", first name is this "test")
			//also I added if statements to be able to write in any order
			//you can write first username then first and last names or otherwise how do you like it
			if(fnA.substring(0, 4).equalsIgnoreCase("-fn="))
				fn = fnA.substring(5, fnA.length()-1);
			if(fnA.substring(0, 4).equalsIgnoreCase("-ln="))
				ln = fnA.substring(5, fnA.length()-1);
			else
				un = fnA.substring(5, fnA.length()-1);
			
			if(lnA.substring(0, 4).equalsIgnoreCase("-ln="))
				ln = lnA.substring(5, lnA.length()-1);
			if(lnA.substring(0, 4).equalsIgnoreCase("-fn="))
				fn = lnA.substring(5, lnA.length()-1);
			if(lnA.substring(0, 4).equalsIgnoreCase("-un="))
				un = lnA.substring(5, lnA.length()-1);
			
			if(unA.substring(0, 4).equalsIgnoreCase("-un="))
				un = unA.substring(5, unA.length()-1);
			if(unA.substring(0, 4).equalsIgnoreCase("-fn="))
				fn = unA.substring(5, unA.length()-1);
			if(unA.substring(0, 4).equalsIgnoreCase("-ln="))
				ln = unA.substring(5, unA.length()-1);
			//creating insert statement
			String query = "Insert into user(FirstName, LastName, Username)\r\n"
	    			+ "values ('"+fn+"', '"+ln+"','"+un+"');";
			//execute statement
			try {
	        	PreparedStatement pstmt = connection.prepareStatement(query);
				pstmt.execute();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		private static void showUsers(Connection connection) throws SQLException {
			//select statement
			String query = "SELECT user.FirstName, user.LastName, COUNT(tasks.idtasks) FROM tasks\r\n"
					+ "LEFT JOIN user ON user.Username= tasks.Username\r\n"
					+ "GROUP BY user.FirstName;";
			Statement st = connection.createStatement();
			ResultSet resultSet = st.executeQuery(query);
			//display result
			while (resultSet.next()) {
				System.out.println("FirstName="+resultSet.getString(1)+" LastName="+resultSet.getString(2)+" Tasks="+resultSet.getString(3));
	        }
		}

		private static void addTask(String[] args, Connection connection) {
			String unA = args[1];
			String ttA = args[2];
			String tdA = args[3];
			String un="";
			//select only the username, not all argument (e.g. if argument is this "-un='test'", username is this "test")
			un = unA.substring(5, unA.length()-1);
			
			String query = "Insert into tasks(TaskTitle, TaskDescription, Username)\r\n"
					+ "values ('"+ttA.substring(5, ttA.length()-1)+"', '"+tdA.substring(5, tdA.length()-1)+"','"+un+"');";
			try {
	        	PreparedStatement pstmt = connection.prepareStatement(query);
				pstmt.execute();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		private static void showTasks(String[] args, Connection connection) throws SQLException {
			//read second argument
			String unA = args[1];
			String un="";
			//select only the username, not all argument (e.g. if argument is this "-un='test'", username is this "test")
			un = unA.substring(5, unA.length()-1);
			String query = "SELECT TaskTitle, TaskDescription FROM taskmanager.tasks WHERE Username='"+un+"';";
			Statement st = connection.createStatement();
			ResultSet resultSet = st.executeQuery(query);
			while (resultSet.next()) {
				System.out.println("TaskTitles="+resultSet.getString(1)+" TaskDescription="+resultSet.getString(2));
	        }
		}
		
		private static void groupTask(String[] args,Connection connection) throws SQLException {
			String tt = args[1];
			String query = "SELECT user.FirstName, user.LastName FROM taskmanager.tasks\r\n"
					+ "LEFT JOIN user ON user.Username= tasks.Username\r\n"
					+ "WHERE tasks.TaskTitle='"+tt.substring(5, tt.length()-1)+"';";
			Statement st = connection.createStatement();
			ResultSet resultSet = st.executeQuery(query);
			
			while (resultSet.next()) {
				System.out.println("FirstName="+resultSet.getString(1)+" LastName="+resultSet.getString(2));
	        }
		}
		
	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub
		final String URL = "jdbc:mysql://localhost:3306/taskmanager";
		final String USER = "stas";
		final String PASSWORD = "1234";
		
		Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
		if (connection == null) {
	         System.out.println("JDBC connection error");
	         return;
	      } else
	    	  System.out.println("JDBC connection success");
		
		String method = args[0];
		if(method.equalsIgnoreCase("-createUser"))
			createUser(args, connection);
		if(method.equalsIgnoreCase("-showAllUsers"))
			showUsers(connection);
		if(method.equalsIgnoreCase("-addTask"))
			addTask(args,connection);
		if(method.equalsIgnoreCase("-showTasks"))
			showTasks(args,connection);
		if(method.equalsIgnoreCase("-groupTasks"))
			groupTask(args,connection);
	}

}
