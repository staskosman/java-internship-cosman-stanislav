package Exercice1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
	int[] candles;
	static int n, i, count=0;
	
	private static int birthdayCakeCandles(int[] candles, int n) {
		int max=0;
		//find the tallest candle
		for(i=0;i<n;i++) {
			if(candles[i]>max) {
				max=candles[i];
			}
		}
		//count how many candles are the tallest 
		for(i=0;i<n;i++) {
			if(candles[i]==max) {
				count++;
			}
		}
		
		return count;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input=new Scanner(System.in);
		
		System.out.println("Please write how many candles cake will have:");
		try {
			n=input.nextInt();
			if(n<1 || n>1000)
				throw new IllegalArgumentException();
		} catch (InputMismatchException e) {
			System.out.println("Please write int");
		}
		
		int[] candles=new int[n];
		//insert height of candles
		for(i=0;i<n;i++) {
			try {
				candles[i]=input.nextInt();
			} catch (InputMismatchException e) {
				System.out.println("Please write int");
			}
		}
		
		System.out.println(birthdayCakeCandles(candles, n));
		
		input.close();
	}

}
