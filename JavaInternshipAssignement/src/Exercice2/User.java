package Exercice2;

import java.io.Serializable;

public class User implements Serializable{
	String FirstName, LastName, userName, TaskTitle, TaskDescription;
	User(String FirstName, String LastName, String userName, String TaskTitle, String TaskDescription){
		this.FirstName=FirstName;
		this.LastName=LastName;
		this.userName=userName;
		this.TaskTitle=TaskTitle;
		this.TaskDescription=TaskDescription;
	}
	
	@Override
	public String toString() {
		return "User [FirstName=" + FirstName + ", LastName=" + LastName + ", userName=" + userName + ", TaskTitle="
				+ TaskTitle + ", TaskDescription=" + TaskDescription + "]";
	}
}
