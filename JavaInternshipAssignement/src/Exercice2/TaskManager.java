package Exercice2;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class TaskManager {
	
	//this method write in file user.ser
	private static void fileWrite(List<User> u1) {
		try {
			FileOutputStream file = new FileOutputStream("user.ser");
			ObjectOutputStream out=new ObjectOutputStream(file);
			
			out.writeObject(u1);
			
			out.close();
			file.close();
		} catch (Exception e) {
			e.printStackTrace();
		}         
	}
	//this method read from file user.ser
	private static List<User> fileRead() {
		//all users will be stocked in list
		List<User> users = null;
        
        try {
        	//name of file
            FileInputStream file = new FileInputStream("user.ser");
            ObjectInputStream in = new ObjectInputStream(file);
            //reding and saving users in ArrayList
            users = (ArrayList<User>) in.readObject();
 
            in.close();
            file.close();
        } 
        catch (IOException ioe) {
            ioe.printStackTrace();
        } 
        catch (ClassNotFoundException c) {
            System.out.println("Class not found");
            c.printStackTrace();
        }   
        return users;     
	}
	//this method show all users from file
	private static void showUsers() {
		List<User> users=fileRead();
		for (User u : users) {
	        System.out.println(u);
	    } 
	}
	
	//this method can create user
	private static void createUser(String[] args) {
		//store users in ArrayList
		List<User> users = new ArrayList<User>();
		//create some users and add in list
		//I decided to do this because I had trouble with introducing more then 1 user in file
		User u2 = new User("test2","test2","test2","Task2", "Description Task2");
		User u3 = new User("test3","test3","test3","Until without task", "no task");
		users.add(u2);
		users.add(u3);
		//read second, third and fourth argument
		String fnA = args[1];
		String lnA = args[2];
		String unA = args[3];
		
		String fn = "";
		String ln = "";
		String un = "";
		//select only the first name, not all argument (e.g. if argument is this "-fn='test'", first name is this "test")
		//also I added if statements to be able to write in any order
		//you can write first username then first and last names or otherwise how do you like it
		if(fnA.substring(0, 4).equalsIgnoreCase("-fn="))
			fn = fnA.substring(5, fnA.length()-1);
		if(fnA.substring(0, 4).equalsIgnoreCase("-ln="))
			ln = fnA.substring(5, fnA.length()-1);
		else
			un = fnA.substring(5, fnA.length()-1);
		
		if(lnA.substring(0, 4).equalsIgnoreCase("-ln="))
			ln = lnA.substring(5, lnA.length()-1);
		if(lnA.substring(0, 4).equalsIgnoreCase("-fn="))
			fn = lnA.substring(5, lnA.length()-1);
		else
			un = lnA.substring(5, lnA.length()-1);
		
		if(unA.substring(0, 4).equalsIgnoreCase("-un="))
			un = unA.substring(5, unA.length()-1);
		if(unA.substring(0, 4).equalsIgnoreCase("-fn="))
			fn = unA.substring(5, unA.length()-1);
		else
			ln = unA.substring(5, unA.length()-1);
		
		//create new User, add new user in list and file user.ser
		User u1 = new User(fn,ln,un,"Until without task", "no task");
		users.add(u1);
		fileWrite(users);		
	}

	//this method can add some task to user
	public static void addTask(String[] args) {
		//reading from file and storage List<>
		List<User> users=fileRead();
		//read second, third and fourth argument
		String unA = args[1];
		String ttA = args[2];
		String tdA = args[3];
		
		String un="";
		//select only the username, not all argument (e.g. if argument is this "-un='test'", username is this "test")
		un = unA.substring(5, unA.length()-1);
		//search in List<User> right username and writing title and description of task
		for(User u:users) {
			if(u.userName.equalsIgnoreCase(un)) {
				//select only the task, not all argument (e.g. if argument is this "-tt='Task1'", task is this "Task1")
				u.TaskTitle = ttA.substring(5, ttA.length()-1);
				u.TaskDescription = tdA.substring(5, tdA.length()-1);
			}
		}
		//writing in file user.ser
		fileWrite(users);
		
	}
	//this method showTask in dependency of username
	private static void showTasks(String[] args) {
		//reading from file and storage List<>
		List<User> users=fileRead();
		//read second argument
		String unA = args[1];
		String un="";
		
		//select only the username, not all argument (e.g. if argument is this "-un='test'", username is this "test")
		un = unA.substring(5, unA.length()-1);
		//search in List<User> right username and display task
		for(User u:users) {
			if(u.userName.equalsIgnoreCase(un)) {
				System.out.println("TaskTitle="+u.TaskTitle+" TaskDescription="+u.TaskDescription);
			}
		}	
	}
	
	public static void main(String[] args) {
		//choosing methods
		String method = args[0];
		if(method.equalsIgnoreCase("-createUser"))
			createUser(args);
		if(method.equalsIgnoreCase("-showAllUsers"))
			showUsers();
		if(method.equalsIgnoreCase("-addTask"))
			addTask(args);
		if(method.equalsIgnoreCase("-showTasks"))
			showTasks(args);
	}
}
